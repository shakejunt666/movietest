package ru.nikitadrzh.view.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ru.nikitadrzh.githubusersearch.R
import ru.nikitadrzh.view.injection.DaggerInitializer

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        DaggerInitializer.initActivityComponent()
    }
}