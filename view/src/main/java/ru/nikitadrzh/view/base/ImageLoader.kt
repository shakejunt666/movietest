package ru.nikitadrzh.view.base

import android.widget.ImageView
import com.squareup.picasso.Picasso
import ru.nikitadrzh.githubusersearch.R

class ImageLoader {
    companion object {
        fun downloadPicture(url: String, targetView: ImageView) {
            Picasso.get()
                .load("https://image.tmdb.org/t/p/w500$url")
                .error(R.color.colorPrimary)
                .into(targetView)
        }
    }
}