package ru.nikitadrzh.view.injection.movie_details.modules

import dagger.Module
import dagger.Provides
import ru.nikitadrzh.presentation.movie_details.MovieDetailsPresenter
import ru.nikitadrzh.presentation.movie_details.MovieDetailsView
import ru.nikitadrzh.view.injection.DaggerInitializer

@Module
class MovieDetailsPresentersModule(val detailsView: MovieDetailsView) {

    @Provides
    fun provideMoviesDetailsPresenter(): MovieDetailsPresenter {
        val presenter = MovieDetailsPresenter(detailsView)
        DaggerInitializer.detailsSubComponent.inject(presenter)
        return presenter
    }
}