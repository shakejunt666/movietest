package ru.nikitadrzh.view.injection.modules

import dagger.Module
import dagger.Provides
import ru.nikitadrzh.view.movies_search.MoviesRecyclerAdapter

@Module
class RecyclerModule {

    @Provides
    fun provideMoviesRecyclerAdapter(): MoviesRecyclerAdapter {
        return MoviesRecyclerAdapter()
    }
}