package ru.nikitadrzh.view.injection

import ru.nikitadrzh.view.injection.modules.InteractorsModule
import ru.nikitadrzh.view.injection.modules.MappersModule
import ru.nikitadrzh.view.injection.modules.RecyclerModule
import ru.nikitadrzh.view.injection.modules.SchedulerModule
import ru.nikitadrzh.view.injection.movie_details.DetailsComponent
import ru.nikitadrzh.view.injection.movie_details.modules.MovieDetailsPresentersModule
import ru.nikitadrzh.view.injection.movie_search.SearchComponent
import ru.nikitadrzh.view.injection.movie_search.modules.MovieSearchPresentersModule
import ru.nikitadrzh.view.movie_details.MoviesDetailsFragment
import ru.nikitadrzh.view.movies_search.MoviesSearchFragment

class DaggerInitializer {

    companion object {
        lateinit var activityComponent: ActivityComponent
        lateinit var searchSubComponent: SearchComponent
        lateinit var detailsSubComponent: DetailsComponent

        fun initActivityComponent(): ActivityComponent {
            activityComponent = DaggerActivityComponent.builder()
                .interactorsModule(InteractorsModule())
                .mappersModule(MappersModule())
                .recyclerModule(RecyclerModule())
                .schedulerModule(SchedulerModule())
                .build()
            return activityComponent
        }

        fun initSearchComponent(moviesSearchFragment: MoviesSearchFragment): SearchComponent {
            searchSubComponent = activityComponent.searchComponent(
                MovieSearchPresentersModule(
                    moviesSearchFragment
                )
            )
            return searchSubComponent
        }

        fun initDetailsComponent(moviesDetailsFragment: MoviesDetailsFragment): DetailsComponent {
            detailsSubComponent = activityComponent.detailsComponent(
                MovieDetailsPresentersModule(
                    moviesDetailsFragment
                )
            )
            return detailsSubComponent
        }
    }
}