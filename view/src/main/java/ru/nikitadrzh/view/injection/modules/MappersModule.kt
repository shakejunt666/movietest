package ru.nikitadrzh.view.injection.modules

import dagger.Module
import dagger.Provides
import ru.nikitadrzh.data.movie_search.mapper.ResponseMapper

@Module
class MappersModule {

    @Provides
    fun provideResponseMapper(): ResponseMapper {
        return ResponseMapper()
    }
}