package ru.nikitadrzh.view.injection.modules

import dagger.Module
import dagger.Provides
import ru.nikitadrzh.data.movie_search.mapper.ResponseMapper
import ru.nikitadrzh.data.movie_search.repository.MoviesRepositoryImpl
import ru.nikitadrzh.domain.movies_search.interactor.FindMoviesInteractor
import ru.nikitadrzh.domain.movies_search.repository.MoviesRepository

@Module
class InteractorsModule {

    @Provides
    fun provideMoviesRepository(mapper: ResponseMapper): MoviesRepository {
        return MoviesRepositoryImpl(mapper)
    }

    @Provides
    fun provideFindMoviesInteractor(moviesRepository: MoviesRepository): FindMoviesInteractor {
        return FindMoviesInteractor(moviesRepository)
    }
}