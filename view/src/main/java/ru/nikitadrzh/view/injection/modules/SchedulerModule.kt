package ru.nikitadrzh.view.injection.modules

import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers

@Module
class SchedulerModule {

    @Provides
    fun provideAndroidSchedulers(): Scheduler {
        return AndroidSchedulers.mainThread()
    }
}