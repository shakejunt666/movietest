package ru.nikitadrzh.view.injection.movie_search

import dagger.Subcomponent
import ru.nikitadrzh.presentation.movies_search.MoviesSearchPresenter
import ru.nikitadrzh.view.injection.movie_search.modules.MovieSearchPresentersModule
import ru.nikitadrzh.view.movies_search.MoviesSearchFragment

@Subcomponent(modules = [MovieSearchPresentersModule::class])
interface SearchComponent {
    fun inject(moviesSearchPresenter: MoviesSearchPresenter)
    fun inject(moviesSearchFragment: MoviesSearchFragment)
}