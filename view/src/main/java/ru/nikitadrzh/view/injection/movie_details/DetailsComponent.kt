package ru.nikitadrzh.view.injection.movie_details

import dagger.Subcomponent
import ru.nikitadrzh.presentation.movie_details.MovieDetailsPresenter
import ru.nikitadrzh.view.injection.movie_details.modules.MovieDetailsPresentersModule
import ru.nikitadrzh.view.movie_details.MoviesDetailsFragment

@Subcomponent(modules = [MovieDetailsPresentersModule::class])
interface DetailsComponent {
    fun inject(movieDetailsPresenter: MovieDetailsPresenter)
    fun inject(moviesDetailsFragment: MoviesDetailsFragment)
}