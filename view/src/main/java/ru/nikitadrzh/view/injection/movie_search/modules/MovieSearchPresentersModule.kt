package ru.nikitadrzh.view.injection.movie_search.modules

import dagger.Module
import dagger.Provides
import ru.nikitadrzh.presentation.movie_details.MovieDetailsPresenter
import ru.nikitadrzh.presentation.movie_details.MovieDetailsView
import ru.nikitadrzh.presentation.movies_search.MoviesSearchPresenter
import ru.nikitadrzh.presentation.movies_search.MoviesSearchView
import ru.nikitadrzh.view.injection.DaggerInitializer

@Module
class MovieSearchPresentersModule(val searchView: MoviesSearchView) {

    @Provides
    fun provideMoviesSearchPresenter(): MoviesSearchPresenter {
        val presenter = MoviesSearchPresenter(searchView)
        DaggerInitializer.searchSubComponent.inject(presenter)
        return presenter
    }
}
