package ru.nikitadrzh.view.injection

import dagger.Component
import ru.nikitadrzh.view.injection.modules.InteractorsModule
import ru.nikitadrzh.view.injection.modules.MappersModule
import ru.nikitadrzh.view.injection.modules.RecyclerModule
import ru.nikitadrzh.view.injection.modules.SchedulerModule
import ru.nikitadrzh.view.injection.movie_details.DetailsComponent
import ru.nikitadrzh.view.injection.movie_details.modules.MovieDetailsPresentersModule
import ru.nikitadrzh.view.injection.movie_search.SearchComponent
import ru.nikitadrzh.view.injection.movie_search.modules.MovieSearchPresentersModule

@Component(
    modules = [InteractorsModule::class, RecyclerModule::class, SchedulerModule::class, MappersModule::class]
)
interface ActivityComponent {
    fun detailsComponent(detailsPresenterModule: MovieDetailsPresentersModule): DetailsComponent
    fun searchComponent(searchPresentersModule: MovieSearchPresentersModule): SearchComponent
}