package ru.nikitadrzh.view.movie_details

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_movies_details.*
import ru.nikitadrzh.githubusersearch.R
import ru.nikitadrzh.presentation.movie_details.MovieDetailsPresenter
import ru.nikitadrzh.presentation.movie_details.MovieDetailsView
import ru.nikitadrzh.view.base.ImageLoader
import ru.nikitadrzh.view.injection.DaggerInitializer
import javax.inject.Inject

class MoviesDetailsFragment : Fragment(), MovieDetailsView {
    @Inject
    lateinit var movieDetailsPresenter: MovieDetailsPresenter

    /**
     * Жизненный цикл
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerInitializer.initDetailsComponent(this).inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_movies_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        movieDetailsPresenter.showMovieDetails()
        back_button.setOnClickListener { movieDetailsPresenter.goToMoviesSearch() }
    }

    /**
     * Методы DetailsView
     */
    override fun showDetails() {
        detailed_title_view.text = arguments?.get("movieTitle") as String
        detailed_overview.text = arguments?.get("movieOverview") as String
        detailed_overview.movementMethod = ScrollingMovementMethod()
        ImageLoader.downloadPicture(arguments?.get("moviePoster") as String, detailed_poster)
    }

    override fun showMoviesSearch() {
        findNavController().popBackStack()
    }
}