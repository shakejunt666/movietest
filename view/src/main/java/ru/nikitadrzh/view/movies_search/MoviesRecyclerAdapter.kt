package ru.nikitadrzh.view.movies_search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import kotlinx.android.synthetic.main.movie_view.view.*
import ru.nikitadrzh.domain.movies_search.model.Movie
import ru.nikitadrzh.githubusersearch.R
import ru.nikitadrzh.view.base.ImageLoader
import java.util.concurrent.TimeUnit

class MoviesRecyclerAdapter :
    RecyclerView.Adapter<MoviesRecyclerAdapter.SearchResultViewHolder>() {

    var moviesList: MutableList<Movie> = ArrayList()
    private val movieClickSubject = BehaviorSubject.create<Movie>()

    override fun getItemCount(): Int {
        return moviesList.size
    }

    override fun onBindViewHolder(holder: SearchResultViewHolder, position: Int) {
        holder.setItem(moviesList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchResultViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.movie_view, parent, false)
        return SearchResultViewHolder(itemView, movieClickSubject)
    }

    fun onMoviesListUpdates(movieList: List<Movie>) {
        this.moviesList.addAll(movieList)
        notifyDataSetChanged()
    }

    fun onMovieClicked(): Observable<Movie> {
        return movieClickSubject.throttleFirst(300L, TimeUnit.MILLISECONDS)
    }

    /**
     * ViewHolder
     */
    class SearchResultViewHolder(itemView: View, private val movieClickSubject: Subject<Movie>) :
        RecyclerView.ViewHolder(itemView) {

        private lateinit var movie: Movie

        fun setItem(movie: Movie) {
            this.movie = movie
            itemView.search_title_view.text = movie.title
            itemView.setOnClickListener { movieClickSubject.onNext(movie) }
            ImageLoader.downloadPicture(movie.posterUrl, itemView.search_movie_view)
        }
    }
}