package ru.nikitadrzh.view.movies_search

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_movies_search.*
import ru.nikitadrzh.domain.movies_search.model.Movie
import ru.nikitadrzh.githubusersearch.R
import ru.nikitadrzh.presentation.movies_search.MoviesSearchPresenter
import ru.nikitadrzh.presentation.movies_search.MoviesSearchView
import ru.nikitadrzh.view.injection.DaggerInitializer
import javax.inject.Inject

class MoviesSearchFragment : Fragment(), MoviesSearchView {
    @Inject
    lateinit var moviesRecyclerAdapter: MoviesRecyclerAdapter

    @Inject
    lateinit var moviesSearchPresenter: MoviesSearchPresenter

    lateinit var movieClickDisposable: Disposable

    /**
     * Жизненный цикл
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerInitializer.initSearchComponent(this).inject(this)
        retainInstance = true
        moviesSearchPresenter.findMovies()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_movies_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        if (moviesRecyclerAdapter.moviesList.isEmpty()) progress_bar.visibility = View.VISIBLE
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::movieClickDisposable.isInitialized) movieClickDisposable.dispose()
    }

    /**
     * MoviesSearchView
     */
    override fun showMovies(movies: List<Movie>) {
        moviesRecyclerAdapter.onMoviesListUpdates(movies)
        progress_bar.visibility = View.GONE
    }

    override fun showError(error: Throwable) {
        Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show()
        progress_bar.visibility = View.GONE
    }

    override fun showMovieDetails(movie: Movie) {
        val movieBundle = Bundle()
        movieBundle.putString("movieTitle", movie.title)
        movieBundle.putString("movieOverview", movie.overview)
        movieBundle.putString("moviePoster", movie.posterUrl)
        findNavController().navigate(R.id.toDetails, movieBundle)
    }

    /**
     * RecyclerInit
     */
    private fun initRecyclerView() {
        if (search_recycler.adapter == null) {
            search_recycler.adapter = moviesRecyclerAdapter
        } else {
            moviesRecyclerAdapter = search_recycler.adapter as MoviesRecyclerAdapter
        }
        if (search_recycler.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT)
            search_recycler.layoutManager = GridLayoutManager(context, 2)
        else if (search_recycler.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE)
            search_recycler.layoutManager = GridLayoutManager(context, 4)

        if (!::movieClickDisposable.isInitialized)
            movieClickDisposable = moviesRecyclerAdapter.onMovieClicked()
                .subscribe(moviesSearchPresenter::goToMoviesDetails, this::showError)
    }
}