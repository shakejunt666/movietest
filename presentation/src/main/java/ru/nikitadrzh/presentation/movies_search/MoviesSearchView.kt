package ru.nikitadrzh.presentation.movies_search

import ru.nikitadrzh.domain.movies_search.model.Movie

interface MoviesSearchView {
    fun showMovies(movies: List<Movie>)

    fun showError(error: Throwable)

    fun showMovieDetails(movie: Movie)
}