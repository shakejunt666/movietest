package ru.nikitadrzh.presentation.movies_search

import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ru.nikitadrzh.domain.movies_search.interactor.FindMoviesInteractor
import ru.nikitadrzh.domain.movies_search.model.Movie
import javax.inject.Inject

class MoviesSearchPresenter(private val view: MoviesSearchView) {
    private lateinit var findMoviesDisposable: Disposable

    @Inject
    lateinit var findMoviesInteractor: FindMoviesInteractor

    @Inject
    lateinit var androidScheduler: Scheduler

    fun findMovies() {
        findMoviesDisposable = findMoviesInteractor.execute()
            .subscribeOn(Schedulers.io())
            .observeOn(androidScheduler)
            .subscribe(this::showMovies, this::showError)
    }

    fun goToMoviesDetails(movie: Movie) {
        view.showMovieDetails(movie)
    }

    private fun showMovies(movies: List<Movie>) {
        view.showMovies(movies)
        findMoviesDisposable.dispose()
    }

    private fun showError(error: Throwable) {
        view.showError(error)
        findMoviesDisposable.dispose()
    }
}