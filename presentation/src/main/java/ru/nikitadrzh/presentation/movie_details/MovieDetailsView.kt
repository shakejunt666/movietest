package ru.nikitadrzh.presentation.movie_details

interface MovieDetailsView {
    fun showDetails()

    fun showMoviesSearch()
}