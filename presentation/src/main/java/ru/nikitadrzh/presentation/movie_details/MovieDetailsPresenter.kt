package ru.nikitadrzh.presentation.movie_details

class MovieDetailsPresenter(private val view: MovieDetailsView) {

    fun showMovieDetails() {
        view.showDetails()
    }

    fun goToMoviesSearch() {
        view.showMoviesSearch()
    }
}