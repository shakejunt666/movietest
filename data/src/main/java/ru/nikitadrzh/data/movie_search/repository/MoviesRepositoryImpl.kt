package ru.nikitadrzh.data.movie_search.repository

import io.reactivex.Single
import ru.nikitadrzh.data.base.RetrofitInitializer
import ru.nikitadrzh.data.movie_search.mapper.ResponseMapper
import ru.nikitadrzh.data.movie_search.service.MoviesService
import ru.nikitadrzh.domain.movies_search.model.Movie
import ru.nikitadrzh.domain.movies_search.repository.MoviesRepository

class MoviesRepositoryImpl(private val mapper: ResponseMapper) : MoviesRepository {

    private val moviesService: MoviesService = RetrofitInitializer.moviesService

    override fun getMovies(): Single<List<Movie>> {
        return moviesService.getMoviesList(RetrofitInitializer.apiKey, 1)
            .map { mapper.mapResponseToMovies(it) }
    }
}