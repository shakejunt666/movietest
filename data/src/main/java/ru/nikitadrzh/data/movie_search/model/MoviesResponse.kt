package ru.nikitadrzh.data.movie_search.model

class MoviesResponse(
    var results: List<MovieResponse>
) {
    class MovieResponse(
        val title: String,
        val overview: String,
        val poster_path: String
    )
}