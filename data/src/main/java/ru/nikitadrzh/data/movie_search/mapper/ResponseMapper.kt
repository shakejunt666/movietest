package ru.nikitadrzh.data.movie_search.mapper

import ru.nikitadrzh.data.movie_search.model.MoviesResponse
import ru.nikitadrzh.domain.movies_search.model.Movie

class ResponseMapper {

    fun mapResponseToMovies(response: MoviesResponse): List<Movie> {
        val listMovies: MutableList<Movie> = ArrayList()
        for (movieResponse in response.results) {
            listMovies.add(
                Movie(
                    movieResponse.title, movieResponse.overview, movieResponse.poster_path
                )
            )
        }
        return listMovies
    }
}