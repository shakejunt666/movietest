package ru.nikitadrzh.data.movie_search.service

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import ru.nikitadrzh.data.movie_search.model.MoviesResponse

interface MoviesService {

    @GET("3/movie/popular")
    fun getMoviesList(
        @Query("api_key") apiKey: String,
        @Query("page") page: Int
    ): Single<MoviesResponse>
}