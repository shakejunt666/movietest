package ru.nikitadrzh.domain.movies_search.model

class Movie(
    val title: String,
    val overview: String,
    val posterUrl: String
)