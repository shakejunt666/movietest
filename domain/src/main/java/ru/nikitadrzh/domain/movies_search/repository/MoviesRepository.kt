package ru.nikitadrzh.domain.movies_search.repository

import io.reactivex.Single
import ru.nikitadrzh.domain.movies_search.model.Movie

interface MoviesRepository {
    fun getMovies(): Single<List<Movie>>
}