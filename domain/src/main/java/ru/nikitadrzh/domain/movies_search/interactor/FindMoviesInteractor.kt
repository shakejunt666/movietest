package ru.nikitadrzh.domain.movies_search.interactor

import io.reactivex.Single
import ru.nikitadrzh.domain.movies_search.model.Movie
import ru.nikitadrzh.domain.movies_search.repository.MoviesRepository

class FindMoviesInteractor(private val moviesRepository: MoviesRepository) {

    fun execute(): Single<List<Movie>> {
        return moviesRepository.getMovies()
    }
}