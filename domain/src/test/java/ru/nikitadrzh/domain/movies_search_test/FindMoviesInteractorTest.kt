package ru.nikitadrzh.domain.movies_search_test

import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock
import ru.nikitadrzh.domain.movies_search.interactor.FindMoviesInteractor
import ru.nikitadrzh.domain.movies_search.model.Movie
import ru.nikitadrzh.domain.movies_search.repository.MoviesRepository

class FindMoviesInteractorTest {
    lateinit var moviesRepository: MoviesRepository
    lateinit var findMoviesInteractor: FindMoviesInteractor
    lateinit var testObserver: TestObserver<List<Movie>>

    val testMovie: Movie =
        Movie("Shrek", "overview", "pictureUrl")

    @Before
    fun setUp() {
        moviesRepository = mock(MoviesRepository::class.java)
        findMoviesInteractor =
            FindMoviesInteractor(moviesRepository)
        testObserver = TestObserver.create()
    }

    @Test
    fun shouldReturnListOfMovies() {
        val listOfUsers: MutableList<Movie> = ArrayList()
        listOfUsers.add(testMovie)

        Mockito.`when`(moviesRepository.getMovies()).thenReturn(Single.just(listOfUsers))
        findMoviesInteractor.execute().subscribe(testObserver)

        Mockito.verify(moviesRepository, Mockito.times(1)).getMovies()
        Mockito.verifyNoMoreInteractions(moviesRepository)

        testObserver.assertValue(listOfUsers)
    }
}
